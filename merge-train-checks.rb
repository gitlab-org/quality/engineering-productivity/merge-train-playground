#!/usr/bin/env ruby

require 'time'
require 'gitlab'
require 'optparse'

class MergeTrainChecks
  COM_GITLAB_API = 'https://gitlab.com/api/v4'
  PIPELINE_FRESHNESS_THRESHOLD_IN_HOURS = 4

  Gitlab.configure do |config|
    config.endpoint = COM_GITLAB_API
    config.private_token = ENV.fetch('GITLAB_API_PRIVATE_TOKEN', '')
  end

  def initialize(project_id: ENV['CI_PROJECT_ID'], merge_request_iid: ENV['CI_MERGE_REQUEST_IID'], current_pipeline_id: ENV['CI_PIPELINE_ID'])
    @client              = Gitlab.client
    @project_id          = project_id
    @merge_request_iid   = merge_request_iid.to_i
    @current_pipeline_id = current_pipeline_id.to_i

    check_required_ids!
  end

  def execute
    latest_pipeline_id = Gitlab.merge_request_pipelines(project_id, merge_request_iid).auto_paginate do |pipeline|
      next if pipeline.id == current_pipeline_id

      break pipeline.id
    end
    raise "Expected to have a latest pipeline but got none!" unless latest_pipeline_id

    latest_pipeline = Gitlab.pipeline(project_id, latest_pipeline_id)

    check_merge_result_commit_pipeline!(latest_pipeline)
    check_pipeline_freshness!(latest_pipeline)
    check_non_predictive_pipeline!(latest_pipeline)

    puts "All good for merge! 🚀"
  rescue Net::OpenTimeout => e
    puts "Caught a Net::OpenTimeout #{e}. Moving on."
  rescue Gitlab::Error => e
    puts "Caught a GitLab::Error (#{e}). Moving on."
  end

  private

  attr_reader :client, :project_id, :merge_request_iid, :current_pipeline_id

  def check_required_ids!
    raise 'Missing project_id' unless project_id
    raise 'Missing merge_request_iid' if merge_request_iid.zero?
    raise 'Missing current_pipeline_id' if current_pipeline_id.zero?
  end

  def check_merge_result_commit_pipeline!(pipeline)
    return if pipeline.ref.end_with?('/merge')

    raise "Expected to have a merge-result-commit pipeline but got #{pipeline.ref}!"
  end

  def check_pipeline_freshness!(pipeline)
    hours_ago = ((Time.now - Time.parse(pipeline.created_at)) / 3600).ceil(2)
    return if hours_ago < PIPELINE_FRESHNESS_THRESHOLD_IN_HOURS

    raise "Expected latest pipeline to be created within the last 4 hours (it was created #{hours_ago} hours ago)!"
  end

  def check_non_predictive_pipeline!(pipeline)
    return unless pipeline.name.include?('predictive')

    raise <<~MSG
    Expected latest pipeline not to be a predictive pipeline!

    Please ensure the MR has all the required approvals, start a new pipeline and put the MR back on the Merge Train.
    MSG
  end

end

options = {}

OptionParser.new do |opts|
  opts.on("-p", "--project_id [string]", String, "Project ID") do |value|
    options[:project_id] = value
  end

  opts.on("-m", "--merge_request_iid [string]", String, "Merge request IID") do |value|
    options[:merge_request_iid] = value
  end

  opts.on("-c", "--current_pipeline_id [string]", String, "Merge request IID") do |value|
    options[:current_pipeline_id] = value
  end

  opts.on("-h", "--help") do
    puts "Usage: merge-train-checks.rb [--project_id <PROJECT_ID>] [--merge_request_iid <MERGE_REQUEST_IID>] [--current_pipeline_id <CURRENT_PIPELINE_ID>]"
    puts
    puts "Examples:"
    puts
    puts "merge-train-checks.rb --project_id \"gitlab-org/gitlab\" --merge_request_iid \"1\" --current_pipeline_id \"2\""

    exit
  end
end.parse!

MergeTrainChecks.new(**options).execute
